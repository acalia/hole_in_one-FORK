import os
import sys
from distutils.sysconfig import get_python_lib

from setuptools import find_packages, setup

setup(
    name='hole_in_one',
    version='0.0.1',
    packages=find_packages()
    )